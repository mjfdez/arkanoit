﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bricks : MonoBehaviour {

    public int lifes;
    public Material pinkbrick;
    public Material bluebrick;
    public Material greenbrick;


    // Use this for initialization
    void Awake()
    {
        ChangeColor();
    }

    private void ChangeColor()
    {
        if(lifes == 3)
        {
            //color rosa
            GetComponent<Renderer>().material = pinkbrick;
        }
        else if(lifes == 2)
        {
            //color azul
            GetComponent<Renderer>().material = bluebrick;
        }
        else
        {
            //color verde
            GetComponent<Renderer>().material = greenbrick;
        }
    }

    // Update is called once per frame
    public void Touch()
    {
        lifes--;
        if(lifes <= 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            ChangeColor();
        }
    }
}
